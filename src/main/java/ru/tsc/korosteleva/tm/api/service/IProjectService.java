package ru.tsc.korosteleva.tm.api.service;

import ru.tsc.korosteleva.tm.model.Project;

import java.util.List;

public interface IProjectService {

    Project add(Project project);

    Project create(String name);

    Project create(String name, String description);

    List<Project> findAll();

    void remove(Project project);

    void clear();

}
