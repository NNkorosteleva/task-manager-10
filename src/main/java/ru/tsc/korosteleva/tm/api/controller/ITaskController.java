package ru.tsc.korosteleva.tm.api.controller;

public interface ITaskController {

    void createTask();

    void showTaskList();

    void clearTasks();

}
