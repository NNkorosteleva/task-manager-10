package ru.tsc.korosteleva.tm.api.service;

import ru.tsc.korosteleva.tm.model.Command;

public interface ICommandService {

    Command[] getTerminalCommands();

}
