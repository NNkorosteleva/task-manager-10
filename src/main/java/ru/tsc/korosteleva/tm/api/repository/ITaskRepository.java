package ru.tsc.korosteleva.tm.api.repository;

import ru.tsc.korosteleva.tm.model.Task;

import java.util.List;

public interface ITaskRepository {

    Task add(Task task);

    Task create(String name);

    Task create(String name, String description);

    List<Task> findAll();

    void remove(Task task);

    void clear();

}
