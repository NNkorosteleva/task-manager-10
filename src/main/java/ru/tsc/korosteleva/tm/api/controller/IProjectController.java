package ru.tsc.korosteleva.tm.api.controller;

public interface IProjectController {

    void createProject();

    void showProjectList();

    void clearProjects();

}
