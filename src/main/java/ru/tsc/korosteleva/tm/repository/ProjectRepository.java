package ru.tsc.korosteleva.tm.repository;

import ru.tsc.korosteleva.tm.api.repository.IProjectRepository;
import ru.tsc.korosteleva.tm.model.Project;

import java.util.ArrayList;
import java.util.List;

public class ProjectRepository implements IProjectRepository {

    private final List<Project> projects = new ArrayList<Project>();

    @Override
    public Project add(final Project project) {
        projects.add(project);
        return project;
    }

    @Override
    public Project create(final String name) {
        final Project project = new Project();
        project.setName(name);
        return add(project);
    }

    @Override
    public Project create(final String name, final String description) {
        final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        return add(project);
    }

    @Override
    public List<Project> findAll() {
        return projects;
    }

    @Override
    public void remove(Project project) {
        projects.remove(project);
    }

    @Override
    public void clear() {
        projects.clear();
    }

}
