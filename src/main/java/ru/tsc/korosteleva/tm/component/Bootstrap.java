package ru.tsc.korosteleva.tm.component;

import ru.tsc.korosteleva.tm.api.controller.ICommandController;
import ru.tsc.korosteleva.tm.api.controller.IProjectController;
import ru.tsc.korosteleva.tm.api.controller.ITaskController;
import ru.tsc.korosteleva.tm.api.repository.ICommandRepository;
import ru.tsc.korosteleva.tm.api.repository.IProjectRepository;
import ru.tsc.korosteleva.tm.api.repository.ITaskRepository;
import ru.tsc.korosteleva.tm.api.service.ICommandService;
import ru.tsc.korosteleva.tm.api.service.IProjectService;
import ru.tsc.korosteleva.tm.api.service.ITaskService;
import ru.tsc.korosteleva.tm.constant.ArgumentConst;
import ru.tsc.korosteleva.tm.constant.TerminalConst;
import ru.tsc.korosteleva.tm.controller.CommandController;
import ru.tsc.korosteleva.tm.controller.ProjectController;
import ru.tsc.korosteleva.tm.controller.TaskController;
import ru.tsc.korosteleva.tm.repository.CommandRepository;
import ru.tsc.korosteleva.tm.repository.ProjectRepository;
import ru.tsc.korosteleva.tm.repository.TaskRepository;
import ru.tsc.korosteleva.tm.service.CommandService;
import ru.tsc.korosteleva.tm.service.ProjectService;
import ru.tsc.korosteleva.tm.service.TaskService;
import ru.tsc.korosteleva.tm.util.TerminalUtil;

import java.util.Scanner;

public final class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final ITaskController taskController = new TaskController(taskService);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final IProjectController projectController = new ProjectController(projectService);

    public void run(final String[] args) {
        if (processArgument(args)) close();
        showProcess();
    }

    public void showProcess() {
        commandController.showWelcome();
        while (true) {
            System.out.println("ENTER COMMAND:");
            final String command = TerminalUtil.nextLine();
            processCommand(command);
        }
    }

    public void processCommand(final String command) {
        if (command == null || command.isEmpty()) return;
        switch (command) {
            case TerminalConst.ABOUT:
                commandController.showAbout();
                break;
            case TerminalConst.VERSION:
                commandController.showVersion();
                break;
            case TerminalConst.HELP:
                commandController.showHelp();
                break;
            case TerminalConst.INFO:
                commandController.showSystemInfo();
                break;
            case TerminalConst.COMMANDS:
                commandController.showCommands();
                break;
            case TerminalConst.ARGUMENTS:
                commandController.showArguments();
                break;
            case TerminalConst.TASK_CREATE:
                taskController.createTask();
                break;
            case TerminalConst.TASK_LIST:
                taskController.showTaskList();
                break;
            case TerminalConst.TASK_CLEAR:
                taskController.clearTasks();
                break;
            case TerminalConst.PROJECT_CREATE:
                projectController.createProject();
                break;
            case TerminalConst.PROJECT_LIST:
                projectController.showProjectList();
                break;
            case TerminalConst.PROJECT_CLEAR:
                projectController.clearProjects();
                break;
            case TerminalConst.EXIT:
                close();
                break;
            default:
                commandController.showErrorCommand(command);
                break;
        }
    }

    public boolean processArgument(final String[] args) {
        if (args == null || args.length == 0) return false;
        String arg = args[0];
        processArgument(arg);
        return true;
    }

    public void processArgument(final String arg) {
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case ArgumentConst.ABOUT:
                commandController.showAbout();
                break;
            case ArgumentConst.VERSION:
                commandController.showVersion();
                break;
            case ArgumentConst.HELP:
                commandController.showHelp();
                break;
            case ArgumentConst.INFO:
                commandController.showSystemInfo();
                break;
            case ArgumentConst.COMMANDS:
                commandController.showCommands();
                break;
            case ArgumentConst.ARGUMENTS:
                commandController.showArguments();
                break;
            default:
                commandController.showErrorArgument(arg);
                break;
        }
    }

    public void close() {
        System.exit(0);
    }

}
